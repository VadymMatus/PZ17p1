#include "input.hpp"

void pz::cmd_clear()
{
#ifdef _WIN64 // для Windows
	system("cls");
#elif __linux__  // для Linux
	system("clear");
#endif
}


int pz::_getchar()
{
#ifdef _WIN64 // для Windows
	return _getch();
#elif  __linux__  // для Linux
	struct termios newt, oldt;
	tcgetattr(STDIN_FILENO, &oldt);
	newt = oldt;
	newt.c_lflag &= ~(ICANON | ECHO);
	tcsetattr(STDIN_FILENO, TCSANOW, &newt); /* enable raw mode */

	int c = 0;

	for (;;) /* read_symbols */
	{
		c = getchar();
		if (c == 27) /* if ESC */
		{
			c = getchar();
			if (c == '[') /* ... [ */
			{
				c = getchar();
				break;
			}
		}
		break;
	}

	tcsetattr(STDIN_FILENO, TCSANOW, &oldt); /* return old config */
	return c;

#endif 
}

#ifdef __linux__
std::string pz::fground(size_t color, size_t effect)
{
	std::stringstream ss;
	ss << "\033[" << effect << ";" << color << "m";

	return ss.str();
}

std::string pz::fb_end()
{
	std::stringstream ss;
	ss << "\033[0m";

	return ss.str();
}

#endif // __linux__

const bool pz::answer_check()
{
	const char answer(std::cin.get()); // ввод ответа
	std::cin.unget();
	std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n'); // очистка буфера ввода

	return (strchr("y|Y|д|Д|" /*ответы которые показывают согласие*/, answer) != NULL);
}
