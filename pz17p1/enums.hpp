#ifndef ENUM_HPP
#define ENUM_HPP

namespace pz
{

/**
 * @brief Цвета для вывода цветного текста 
 * 
 */
enum Foreground
{
    FG_BLACK = 30,   // чёрный
    FG_RED = 31,     // красный
    FG_GREEN = 32,   // зелёный
    FG_YELLOW = 33,  // желтый
    FG_BLUE = 34,    // синий
    FG_MAGENTA = 35, // фиолетовый
    FG_CYAN = 36,    // океан
    FG_WHITE = 37    // белый
};

/**
 * @brief Вывод цветной подложки
 * 
 */
enum Background
{
    BG_BLACK = 40,   // чёрный
    BG_RED = 41,     // красный
    BG_GREEN = 42,   // зелёный
    BG_YELLOW = 43,  // желтый
    BG_BLUE = 44,    // синий
    BG_MAGENTA = 45, // фиолетовый
    BG_CYAN = 46,    // океан
    BG_WHITE = 47    // белый
};

/**
 * @brief Ефекты выводимого текста
 * 
 */
enum Effect
{
    RESET = 0,          // сброс всех настроек
    BOLD = 1,           // жирный текст
    UNDERLINE = 4,      // подчёркнутый текст
    INVERSE = 7,        // инверсия
    BOLD_OFF = 21,      // отключение жирного текста
    UNDERLINE_OFF = 24, // отключение подчёркивания
    INVERSE_OFF = 27    // отключение инверсии
};

/**
 * @brief Перечисление для удобной работы с стрелками
 * на клавиатуре.
 */
enum
{
#ifdef __linux__ // для Linux
    KEY_UP = 65,
    KEY_DOWN = 66,
    KEY_LEFT = 68,
    KEY_RIGHT = 67,
    ENTER = 10
#elif _WIN64 // для Windows
	KEY_UP = 72,
	KEY_DOWN = 80,
	KEY_LEFT = 75,
	KEY_RIGHT = 77,
	ENTER = 13
#endif
};

} // namespace pz

#endif // ENUM_HPP
